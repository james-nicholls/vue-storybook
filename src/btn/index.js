const Btn = ({ clicked, hover }) => ({
  render (vc) {
    return vc('div', {
      on: {
        click: (evt) => clicked(evt),
        mouseover: (evt) => hover(evt)
      }
    }, ['Hello Moto'])
  }
})

export default Btn

export {

}
