import { configure, storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import Stories from '../src/stories'
import Btn from '../src/btn'

configure(module => {
  storiesOf('Stories', module)
    .add('Test Hello World', () => Stories)

  storiesOf('Btn', module)
    .add('story as a component', () => Btn({
      clicked: action('Clicked'),
      hover: action('Hover')
    }))
}, module)
